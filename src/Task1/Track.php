<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{

    private $lapLength;
    private $lapsNumber;
    private $cars = [];

    public function __construct(float $lapLength, int $lapsNumber)
    {
        //@todo
        if ($lapLength < 0) 
        {
            throw new \Exception('Error! Length of lap is negative!');
        }
        //$this->lapLenght = $lapLenght;
        $this->lapLenght = $lapLength;

        if ($lapsNumber < 0) 
        {
            throw new \Exception('Error! Count of lap must be 1 or greater!');
        }
        $this->lapsNumber = $lapsNumber;

    }

    public function getLapLength(): float
    {
        // @todo
        return $this->lapLenght;
    }

    public function getLapsNumber(): int
    {
        // @todo
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        // @todo
        $this->cars[] = $car;
    }

    public function all(): array
    {
        // @todo
        if (count($this->cars) == 0) 
        {
            throw new \Exception('Any car not exist!');
        }
        return $this->cars;
    }

    public function run(): Car
    {
        // @todo
        if (empty($this->cars)) {
            throw new \Exception("No cars added!");
        }

        $pathLength = $this->lapLength * $this->lapsNumber;
        $carsList = $this->all();
        $winnerRaceTime;


        foreach ($carsList as $car) {

            $raceTimeNoPitstops = $pathLength / $car->getSpeed();
            $carRaceFuel = $pathLength * $car->getFuelConsumption() / 100;

            // Time over all pitstops
            $pitstopsTime = ((int)($carRaceFuel / $car->getFuelTankVolume())) * $car->getPitStopTime();
            $fullRaceTime = $raceTimeNoPitstops + $pitstopsTime;

            if ( isset($winnerRaceTime) === false || $fullRaceTime < $winnerRaceTime ) 
            {
                $winnerRaceTime = $fullRaceTime;
                $winwin = $car;
            } 
            
        }        
        return $winwin;
    }
}
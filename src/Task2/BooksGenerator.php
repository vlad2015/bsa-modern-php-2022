<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private $minPagesNumber;
    private $maxPrice;
    private $libraryBooks;
    private $storeBooks;
    private $filteredListOfBooks = [];

    public function __construct (
        int $minPagesNumber, 
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
        ) 
        {
        if ($maxPrice < 0) {
            throw new \Exception('Error! Price can not be negative');
        } else {
            $this->maxPrice = $maxPrice;
        }
        
        if ($minPagesNumber < 0 ) {
            throw new \Exception('Error! Count  of pages can not be negative');
        } else {
            $this->minPagesNumber = $minPagesNumber;
        }
        $this->libraryBooks = $libraryBooks;
        $this->storeBooks = $storeBooks;

        }

    public function generate(): \Generator
    {
        //@todo
        foreach ($this->libraryBooks as $libraryBook)
        {
            if ($libraryBook->getPagesNumber() >= $this->minPagesNumber)
            {
                $this->filteredListOfBooks[] = $libraryBook;
            }
        }
        foreach($this->storeBooks as $storeBook){
            if ((int)$storeBook->getPrice() <= (int)$this->maxPrice )
            {
                $this->filteredListOfBooks[] = $storeBook;
            }
        }

        foreach($this->filteredListOfBooks as $filteredListOfBook)
        {
            yield $filteredListOfBook;
        }

    }
}
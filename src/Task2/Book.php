<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    private $title;
    private $price;
    private $pagesNumberpagesNumber;

    public function __construct(string $title, int $price, int $pagesNumber)
    {
        $this->title = $title;
        if ($price < 0) {
            throw new \Exception('Error! Price can not be negative');
        } else {
            $this->price = $price;
        }
        
        if ($pagesNumber < 0 ) {
            throw new \Exception('Error! Count  of pages can not be negative');
        } else {
            $this->pagesNumber = $pagesNumber;
        }
    }

    public function getTitle(): string
    {
        // @todo
        return $this->title;
    }

    public function getPrice(): int
    {
        // @todo
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        //@todo
        return $this->pagesNumber;
    }
}